import { useEffect, useState } from "react";
import "./App.css";
import logo from "./logo.svg";

function App() {
  const [user, setUser] = useState();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    try {
      fetch("http://ec2-44-201-198-113.compute-1.amazonaws.com/users")
        .then((response) => response.json())
        .then((data) => {
          console.log(data);
          setUser(data);
          setIsLoading(false);
        });
    } catch (err) {
      setIsLoading(false);
      alert(err);
    }
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        {isLoading ? (
          <>
            <img src={logo} className="App-logo" alt="logo" />
            <p>Loading...</p>
          </>
        ) : (
          <p>
            <p>Name: {user?.name}</p>
            <p>Email: {user?.email}</p>
            <img src={user?.avatar} alt="avatar" />
          </p>
        )}
      </header>
    </div>
  );
}

export default App;
